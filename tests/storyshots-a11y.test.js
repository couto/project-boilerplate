import initStoryshots from '@storybook/addon-storyshots';
import { axe, toHaveNoViolations } from 'jest-axe';
import ReactDOMServer from 'react-dom/server';

expect.extend(toHaveNoViolations);

initStoryshots({
  suite: 'A11Y storyshots',
  test: ({ story, context }) => {
    const storyElement = story.render(context);
    const html = ReactDOMServer.renderToString(storyElement);

    return expect(axe(html)).resolves.toHaveNoViolations();
  }
});
