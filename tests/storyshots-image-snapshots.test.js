import initStoryshots, { imageSnapshot } from '@storybook/addon-storyshots';
import * as path from 'path';
import * as fs from 'fs';

const storybookDir = path.resolve(__dirname, '../storybook-static/');

if (!fs.existsSync(storybookDir)) {
  throw new Error(
    'Please generate a storybook first: yarn run build-storybook',
  );
}

// const beforeScreenshot = () => new Promise(resolve => setTimeout(resolve, 0));

initStoryshots({
  suite: 'Image storyshots',
  test: imageSnapshot({
    storybookUrl: `file://${storybookDir}`,
    // beforeScreenshot,
  }),
});
