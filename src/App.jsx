// @flow

import React from 'react';
import { connect } from 'react-redux';
import { type Dispatch } from 'redux';
import { type Actions, increment, decrement } from './actions';
import { type State } from './reducers';
import Button from './components/atoms/Button';

type Props = {
  count: number,
  onIncrement: NullaryFn<*>,
  onDecrement: NullaryFn<*>,
};

const App = ({ count, onIncrement, onDecrement }: Props) => (
  <div>
    <Button onClick={onIncrement}>+</Button>
    <span>{count}</span>
    <Button onClick={onDecrement}>-</Button>
  </div>
);

const mapStateToProps = (state: State) => ({
  count: state.counter,
});

const mapDispatchToProps = (dispatch: Dispatch<Actions>) => ({
  onIncrement: () => dispatch(increment()),
  onDecrement: () => dispatch(decrement()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
