// @flow

import React from 'react';
import { noop } from 'lodash/fp';
import styled, { type ReactComponentFunctional } from 'styled-components';

type Props = {
  onClick?: NullaryFn<void>,
  children: Node,
};

const Button: ReactComponentFunctional<Props> = styled.button`
  font-size: 14px;
  color: ${props => props.theme.secondaryColor};
  background: ${props => props.theme.primaryColor};
  border: none;
  cursor: pointer;
`;

Button.defaultProps = {
  onClick: noop,
};

export default Button;
