// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { text } from '@storybook/addon-knobs/react';

import Button from './';

storiesOf('Atoms | Button', module).add('Overview', () => (
  <Button onClick={action('onClick')}>{text('children', 'The Label')}</Button>
));
