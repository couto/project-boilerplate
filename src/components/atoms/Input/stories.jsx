// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { text } from '@storybook/addon-knobs/react';

import Button from './';

storiesOf('Atoms | Input', module)
  .add('Overview', () => (
    <Button
      label={text('label', 'LeLabel')}
      message={text('message', 'Hello World')}
      onClick={action('onClick')}
    />
  ))
  .add('with fixed message', () => (
    <Button label="Foo" message="Hello" onClick={action('onClick')} />
  ));
