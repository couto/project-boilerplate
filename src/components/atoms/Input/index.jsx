// @flow

import React from 'react';

type Props = {
  message: string,
  label: string
};

export default ({ message, label }: Props) => (
  <label>
    <span>{label}</span>
    <input value={message} />
  </label>
);
