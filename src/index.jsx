// @flow

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import store from './configureStore';
import App from './App';
import theme from './themes/dark';

const Root = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </Provider>
);

const placeholder = document.getElementById('root');

if (!placeholder) {
  throw new Error('No placeholder to mount the application');
}

render(<Root />, placeholder);
