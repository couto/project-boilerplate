// @flow

export default {
  primaryColor: '#000',
  secondaryColor: '#fff',
};
