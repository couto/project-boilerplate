// @flow

import { type Actions } from './actions';

export type State = {
  counter: number,
};

export const initialState: State = {
  counter: 0,
};

export default (state: State = initialState, action: Actions): State => {
  switch (action.type) {
    case 'INCREMENT':
      return {
        ...state,
        counter: state.counter + 1,
      };
    case 'DECREMENT':
      return {
        ...state,
        counter: state.counter - 1,
      };
    default:
      return state;
  }
};
