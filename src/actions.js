// @flow

type IncrementAction = ActionType<'INCREMENT'>;
type DecrementAction = ActionType<'DECREMENT'>;

export const increment = (): IncrementAction => ({ type: 'INCREMENT' });
export const decrement = (): DecrementAction => ({ type: 'DECREMENT' });

export type Actions = IncrementAction | DecrementAction;
