const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: path.resolve(__dirname, './src/index.jsx'),

  output: {
    path: path.resolve('./build'),
    filename: '[name].[chunkhash].js',
  },

  module: {
    rules: [
      {
        test: /\.jsx?/,
        use: [{ loader: 'babel-loader' }],
        include: path.resolve(__dirname, './src'),
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/index.html'),
    }),
  ],

  resolve: {
    extensions: ['.js', '.json', '.jsx', '.css'],
  },
};
