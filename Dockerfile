# Add git so we can install some dependencies
FROM node:8.10.0-alpine as build
  WORKDIR /app

  RUN apk add --no-cache git
  COPY . .

  # Install all dependencies
  RUN yarn --production=false
  RUN yarn webpack --mode=production

  # Remove devDependencies
  RUN yarn --verbose \
    --production=true \
    --ignore-scripts \
    --prefer-offline \
    --link-duplicates \
    --pure-lockfile


# Build application
FROM node:8.10.0-alpine
  WORKDIR /app

  # Copy minimum amount of files to make the application work
  COPY --from=build /app/build .
  COPY --from=build /app/package.json .
  COPY --from=build /app/node_modules node_modules/

  ENV NODE_ENV production

  EXPOSE 8080
  CMD ["node", "-e", "require('http').createServer((req, res) => res.end('OK')).listen(8080)"]
