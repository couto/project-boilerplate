const ENV = process.env.NODE_ENV;

module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        modules: ENV === 'test' ? 'commonjs' : false,
        targets: {
          browsers: ['last 2 major versions'],
        },
      },
    ],
    '@babel/preset-react',
  ],
  plugins: [
    ENV === 'production'
      ? ['babel-plugin-styled-components', { ssr: true, preprocess: true }]
      : 'babel-plugin-styled-components',
    '@babel/plugin-transform-flow-comments',
    '@babel/plugin-proposal-object-rest-spread',
  ].filter(Boolean),
};
