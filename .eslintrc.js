module.exports = {
  parser: 'babel-eslint',
  env: { browser: true },
  extends: [
    'airbnb',
    'plugin:jest/recommended',
    'plugin:flowtype/recommended',
    'prettier',
  ],
  plugins: ['flowtype'],
  rules: {
    'react/require-default-props': [2, { forbidDefaultForRequired: true }],
    'import/no-extraneous-dependencies': [2, { devDependencies: true }],
  },
};
