import { configure, addDecorator } from '@storybook/react';
import { setOptions } from '@storybook/addon-options';
import { checkA11y } from '@storybook/addon-a11y';
import { withKnobs } from '@storybook/addon-knobs/react';
import { withThemes } from 'storybook-styled-components';
import themes from '../src/themes';

addDecorator(withKnobs);
addDecorator(checkA11y);
addDecorator(withThemes(themes));

setOptions({
  name: require('../package.json').name,
  addonPanelInRight: true,
  sortStoriesByKind: true,
  hierarchySeparator: /\|/,
  sidebarAnimations: true,
});

// automatically import all files ending in *.stories.js
const req = require.context('../src', true, /.stor(y|ies).jsx?$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
