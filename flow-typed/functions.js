// @flow

declare type _ExtractReturn<B, F: (...args: any[]) => B> = B;
declare type ExtractReturn<F> = _ExtractReturn<*, F>;

declare type NullaryFn<O> = () => O;
declare type UnaryFn<I, O> = (I) => O;
declare type BinaryFn<I, A, O> = (I, A) => O;
declare type TernaryFn<I, A, B, O> = (I, A, B) => O;
