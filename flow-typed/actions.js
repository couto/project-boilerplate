// @flow

declare type ActionType<T: $Subtype<string>> = { type: T };
declare type ActionPayload<P> =
  | { payload: P }
  | { payload: Error, error: true };
declare type ActionMeta<M> = { meta: M };
