// flow-typed signature: 69802a2766372a5efda29307d06d0a3e
// flow-typed version: <<STUB>>/jest-axe_v^2.1.4/flow_v0.68.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'jest-axe'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'jest-axe' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module 'jest-axe/index' {
  declare module.exports: $Exports<'jest-axe'>;
}
declare module 'jest-axe/index.js' {
  declare module.exports: $Exports<'jest-axe'>;
}
